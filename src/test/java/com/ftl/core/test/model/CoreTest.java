package com.ftl.core.test.model;

import static com.ftl.core.utils.CoreConstant.WARNING_RAWTYPES;
import static com.ftl.core.utils.CoreConstant.WARNING_UNCHECKED;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;

import com.ftl.core.model.CoreParam;
import com.ftl.core.model.CoreUser;
import com.ftl.core.model.dto.CorePageWrapper;
import com.ftl.core.utils.CoreSpring;


@SuppressWarnings({WARNING_RAWTYPES, WARNING_UNCHECKED})
public class CoreTest {
	
	@Test
	public void creatUrlParams() {
		CoreParam<String> param1 = CoreParam.createCoreParam("id", "33");
		CoreParam<String> param2 = CoreParam.createCoreParam("name", "Petros");
		CoreParam<String> param3 = CoreParam.createCoreParam("class", "support");

		CoreParam[] params = new CoreParam[]{param1, param2, param3};
		
		CorePageWrapper<?> wrapper = new CorePageWrapper(new PageImpl(Arrays.asList(new CoreUser())), "/comments/ajax/get/5/", "qq", params);
		
		assertEquals("/comments/ajax/get/5?id=33&name=Petros&class=support", wrapper.getUrl());
		
	}
	
	@Test
	public void createUrlEmptyParams() {
		CorePageWrapper<?> wrapper = new CorePageWrapper(new PageImpl(Arrays.asList(new CoreUser())), "/comments/ajax/get/5/", "qq");
		
		assertEquals("/comments/ajax/get/5?", wrapper.getUrl());
		
	}
	
	@Test
	public void createSortUrlStringParam() {
		
		List<Order> orders = Arrays.asList(new Order("name"), new Order(Direction.DESC, "createdDate"));
		
		Sort sort = new Sort(orders);
		
		assertEquals("sort=name&sort=createdDate,desc", CoreSpring.getUrlProperty(sort));
	}
	
//	@Test
//	public void createUrlNullParams() {
//		CorePageWrapper<?> wrapper = new CorePageWrapper();
//		wrapper.setUrl("/comments/ajax/get/5/");
//		
//		wrapper.setUrlParams(null);
//		
//		assertEquals("/comments/ajax/get/5?", wrapper.getUrl());
//		
//	}
		
}