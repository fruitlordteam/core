package com.ftl.core.dao;

import java.util.Arrays;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.NaturalIdLoadAccess;
import org.hibernate.Session;

import com.ftl.core.model.CoreModel;
import com.ftl.core.model.CoreParam;
import com.ftl.core.utils.CoreConstant;

/**
 * Extend this class for your custom dao implementations.
 * @author Petros Siatos
 *
 */
public abstract class AbstractCoreDaoCustomImpl<M extends CoreModel> implements ICoreDaoCustom<M> {
	
	@PersistenceContext
	protected EntityManager em;
	
	protected Session getSession() {
		return em.unwrap(Session.class);
	}
	
	public abstract Class<M> getModel();
	
	/** Returns an Optional<M> using Hibernate's Natural ID
	 * <a href = "https://docs.jboss.org/hibernate/orm/5.0/mappingGuide/en-US/html_single/#naturalid">Hibernate Doc</a>
	 * 
	 * @param naturalId the attribute with the NaturalId annotation.
	 * @return
	 */
	public Optional<M> getByNaturalID(CoreParam<?>... params) {
		NaturalIdLoadAccess<M> access = getSession().byNaturalId(getModel());
		Arrays.stream(params).forEach(param -> access.using(param.getAttribute(), param.getValue()));
		
		return Optional.ofNullable(access.load());
	}
	
	/** Returns an Optional<M> using Hibernate's Natural ID
	 * <a href = "https://docs.jboss.org/hibernate/orm/5.0/mappingGuide/en-US/html_single/#naturalid">Hibernate Doc</a>
	 * 
	 * @param naturalId the attribute with the NaturalId annotation.
	 * @return
	 */
	public Optional<M> getBySimpleNaturalID(String naturalId) {
		return Optional.ofNullable(getSession().bySimpleNaturalId(getModel()).load(naturalId));
	}
	
	/**
	 * Checks if a model with same param attribute/values exists in Database
	 * 
	 * TODO: add model in custom DAO like in CoreOldSkoolDao
	 * @param modelClass The class of the model to lookup in db
	 * @param params
	 * @return
	 */
	@SuppressWarnings({CoreConstant.WARNING_RAWTYPES, CoreConstant.WARNING_UNCHECKED})
	public Boolean existsByAttribute(CoreParam<?>... params) {

		if (params == null)
			return false;
		
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery cqry = cb.createQuery();

		Root<M> root = cqry.from(getModel());
		cqry.select(cb.count(root));
		Arrays.stream(params).forEach(param -> cqry.where(cb.equal(root.get(param.getAttribute()), param.getValue())));

		Query query = em.createQuery(cqry);
		Long result = (Long) query.getSingleResult();
		return result > 0;

	}
	
}
