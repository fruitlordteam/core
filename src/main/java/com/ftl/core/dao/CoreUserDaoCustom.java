package com.ftl.core.dao;

import java.util.Optional;

import com.ftl.core.model.CoreUser;

public interface CoreUserDaoCustom extends ICoreDaoCustom<CoreUser> {

	public Optional<CoreUser> getByLogin(String login);
	public Boolean existsByLogin(String login);
	
}
