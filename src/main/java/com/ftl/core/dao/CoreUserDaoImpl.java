package com.ftl.core.dao;

import java.util.Optional;

import com.ftl.core.model.CoreParam;
import com.ftl.core.model.CoreUser;

public class CoreUserDaoImpl extends AbstractCoreDaoCustomImpl<CoreUser> implements CoreUserDaoCustom {

	@Override
	public Class<CoreUser> getModel() { 
		return CoreUser.class; 
	}
	
	@Override
	public Optional<CoreUser> getByLogin(String login) {
		getSession().enableFetchProfile(CoreUser.FETCH_PROFILE_WITH_ROLES);
		return getBySimpleNaturalID(login);
	}
	
	@Override
	public Boolean existsByLogin(String login) {
		return existsByAttribute(CoreParam.createCoreParam(CoreUser.LOGIN, login));
	}

}
