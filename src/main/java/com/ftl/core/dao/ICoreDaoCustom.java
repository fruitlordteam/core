package com.ftl.core.dao;

import com.ftl.core.model.CoreModel;
import com.ftl.core.model.CoreParam;

/**
 * Interface for custom repository functionality
 * <br/>
 * http://docs.spring.io/spring-data/jpa/docs/current/reference/html/#repositories.custom-behaviour-for-all-repositories
 * @author Petros Siatos
 *
 */
public interface ICoreDaoCustom<M extends CoreModel> {

	public Boolean existsByAttribute(CoreParam<?>... params);
	
}