package com.ftl.core.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import com.ftl.core.config.CoreConfig;
import com.ftl.core.model.CoreModel;

/**
 * Core Dao Interface
 * 
 * Uses Spring Data Repository
 * Remember to include {@link CoreConfig}
 * for your daos to work.
 * 
 * @author Petros Siatos
 *
 * @param <M> the Model that needs to extend {@link CoreModel}
 */
@NoRepositoryBean
public interface ICoreDao<M extends CoreModel> extends JpaRepository<M, Long> {
	
	public Page<M> findAll(Pageable pageable);
	
}
