package com.ftl.core.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;

import com.ftl.core.model.CoreUser;

public interface CoreUserDao extends ICoreDao<CoreUser>, CoreUserDaoCustom {

	// Using Entity Graph
	@EntityGraph(value = CoreUser.ENTITY_GRAPH_WITH_ROLES, type = EntityGraphType.LOAD)
	public Optional<CoreUser> findByLogin(String login);
	
}