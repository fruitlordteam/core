package com.ftl.core.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.ftl.core.model.CoreUser;
import com.ftl.core.utils.CoreSecurityAuditorAware;

/**
 * Spring Java Configuration for Core project. Import in your project's
 * configuration @Import({ CoreConfig.class }).
 * 
 * @author Petros Siatos
 *
 */
@Configuration
@ComponentScan({ CoreConfig.PACKAGE_ROOT })
@EnableJpaRepositories({ CoreConfig.PACKAGE_DAO })
// http://docs.spring.io/spring/docs/current/spring-framework-reference/htmlsingle/#aop-proxying
@EnableAspectJAutoProxy(proxyTargetClass = true)
@EnableJpaAuditing
public class CoreConfig {

	public static final String PACKAGE_ROOT = "com.ftl.core";
	public static final String PACKAGE_DAO = "com.ftl.core.dao";

	// JPA / Hibernate
	public static final int ALLOCATION_SIZE = 1;

	@Bean
	public AuditorAware<CoreUser> auditorProvider() {
		return new CoreSecurityAuditorAware();
	}
	
}
