package com.ftl.core.unused;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ftl.core.model.CoreModel;
import com.ftl.core.service.CoreService;

/**
 * Old School Core Service implementation.
 * 
 * @deprecated use {@link CoreService}
 * @author Petros Siatos
 *
 * @param <T> the Model
 * @param <D> the Dao
 */
@Transactional(propagation= Propagation.REQUIRED)
@Deprecated
public abstract class CoreOldSkoolService<T extends CoreModel, D extends CoreOldSkoolDao<T>> {

	private D dao;
	
	public void persist(T obj) {
		dao.persist(obj);
	}
	
	public void save(T obj) {
		dao.save(obj);
	}
	
	public void saveAll(Collection<T> list){
		list.forEach(obj -> save(obj));
	}
	
	public List<T> get(List<Long> ids) {
		return dao.get(ids);
	}
	
	public T get(Long id){
		return dao.get(id);
	}
	
	public List<T> getAll() {
		return dao.getAll();
	}
	
//	public void getOrCreate(T obj){
//		this.dao.getOrCreate(obj);
//	}
	
	@Autowired
	void setDao(D dao){
		this.dao = dao;
	}
	
	public D getDao() {
		return dao;
	}
	
}
