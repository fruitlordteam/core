package com.ftl.core.unused;

import com.ftl.core.model.CoreAudit;
import com.ftl.core.utils.CoreSecurityAuditorAware;

/**
 * A JPA implementation for a listener to set insertDate / updateDate
 * add in model by adding on entity @EntityListeners(CoreModelListener.class)
 * Currently using Spring Data implementation.
 * {@link CoreAudit}
 * and
 * {@link CoreSecurityAuditorAware}
 * @author Petros Siatos
 *
 */
@Deprecated 
public class CoreModelListener {
	
//	@PrePersist
//	public void beforePersist(CoreModel model) {
//		model.setCreatedDate(Calendar.getInstance());
//	}
//	
//	@PreUpdate
//	public void beforeUpdate(CoreModel model) {
//		model.setUpdateDate(Calendar.getInstance());
//	}
	
}
