package com.ftl.core.unused;

import javax.persistence.Column;

import com.ftl.core.annotation.CoreEntity;
import com.ftl.core.model.CoreModel;

/**
 * A simple address to define street name, street no, postal code.
 * @author Petros Siatos
 *
 */
@CoreEntity
//@Entity
//@Table(name= CoreAddress.TABLE)
//@SequenceGenerator(name = CoreModel.ID_SEQ, sequenceName = CoreAddress.SEQ, allocationSize = 1)
public class CoreAddress extends CoreModel{

	public static final String TABLE		= "core_address";
	public static final String SEQ 			= "core_address_seq";
	
	// Parameters
	public static final String STREET_NAME	= "streetName";
	public static final String STREET_NO	= "streetNo";
	public static final String POSTAL_CODE 	= "postalCode";

	// TODO: City?
//	@Column
//	private CoreCity city; 

	// TODO: Use CorePage?
//	@Column
//	@URL
//	private String webPageUri;
	
	@Column
	private String streetName;
	
	@Column
	private Integer streetNo;
	
	@Column
	private String postalCode;

//	public String getWebPageUri() {
//		return webPageUri;
//	}
//
//	public void setWebPageUri(String webPageUri) {
//		this.webPageUri = webPageUri;
//	}

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public Integer getStreetNo() {
		return streetNo;
	}

	public void setStreetNo(Integer streetNo) {
		this.streetNo = streetNo;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	
}
