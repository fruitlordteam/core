package com.ftl.core.unused;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.ftl.core.model.CoreModel;
import com.ftl.core.model.CoreUser;
import com.ftl.core.utils.CoreConstant;

/**
 * Core Model with auditing using Spring Data and Spring Security.
 * @deprecated Core model includes auditing.
 * 
 * <a href = "http://docs.spring.io/spring-data/jpa/docs/current/reference/html/#auditing">Spring Doc</a>
 * @author Petros Siatos
 *
 */
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@Deprecated
abstract public class AbstractCoreAuditedModel extends CoreModel {

	@CreatedBy
	@ManyToOne
	protected CoreUser createdBy;
	
	@LastModifiedBy
	@ManyToOne
	protected CoreUser lastModifiedBy;
	
//	@Temporal(TemporalType.TIMESTAMP)
	@CreatedDate
	@Column(columnDefinition = CoreConstant.COLUMN_DEFINITION_TIMESTAMP)
	protected LocalDateTime createdDate;
	
//	@Temporal(TemporalType.TIMESTAMP)
	@LastModifiedDate
	@Column(columnDefinition = CoreConstant.COLUMN_DEFINITION_TIMESTAMP)
	protected LocalDateTime updateDate;

	public CoreUser getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(CoreUser createdBy) {
		this.createdBy = createdBy;
	}

	public CoreUser getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(CoreUser lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public LocalDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public LocalDateTime getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}
	
}
