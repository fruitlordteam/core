package com.ftl.core.unused;

import java.util.Calendar;

import org.springframework.data.auditing.DateTimeProvider;

public class CoreAuditorDateTimeProvider implements DateTimeProvider {
 
    public CoreAuditorDateTimeProvider() {
    }
 
    @Override
    public Calendar getNow() {
        return Calendar.getInstance();
    }
}
