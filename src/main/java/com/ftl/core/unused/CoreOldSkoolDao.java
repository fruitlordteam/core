package com.ftl.core.unused;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.ftl.core.dao.ICoreDao;
import com.ftl.core.model.CoreModel;
import com.ftl.core.utils.CoreConstant;

/**
 * Old School Core Dao implementation.
 * 
 * @deprecated use {@link ICoreDao}
 * @author Petros Siatos
 *
 * @param <T> the Model
 */
@SuppressWarnings(CoreConstant.WARNING_UNCHECKED)
@Deprecated
abstract public class CoreOldSkoolDao<T extends CoreModel> {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	protected abstract Class<T> getClazz() ;
	
	public void persist(T obj) {
		entityManager.persist(obj);
	}
	
	public void save(T obj) {
		getSession().save(obj);
	}
 
	public T get(Long id){
		return (T) getSession().get(getClazz(),id);
	}
	
	public List<T> get(List<Long> ids) {
		Criteria cr = createCriteria();
		cr.add(Restrictions.in(CoreModel.ID, ids));
		return cr.list();
	}
	
	public List<T> getAll() {
		Criteria cr = createCriteria();
		return cr.list();
	}
	
//	public T getOrCreate(T obj) {
//		T persisted = get(obj);
//		
//		if (persisted == null) {
//			save(obj);
//		}
//		
//		return persisted;
//	}
	
	public Session getSession() {
		return  entityManager.unwrap(Session.class);
	}
	
	protected Criteria createCriteria(){
		return getSession().createCriteria(getClazz());
	}
	
	public EntityManager getEntityManager() {
		return entityManager;
	}

//	protected Criteria createCriteria(){
//		return entityManager.getCriteriaBuilder().set
//	}

	
	
}
