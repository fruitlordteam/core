package com.ftl.core.unused;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.NaturalId;
import org.hibernate.validator.constraints.NotEmpty;

import com.ftl.core.model.CoreModel;

/**
 * Extension of a {@link CoreModel} with a {@link NaturalId} code. 
 * @deprecated just complicates code.
 * 
 * @author Petros Siatos
 *
 */
@MappedSuperclass
@Deprecated
abstract public class CoreModelWithCode extends CoreModel {

	// Attributes
	public static final String CODE = "code";

	@NotEmpty
	@NaturalId
	@Column
	protected String code;
	
	public String getCode() {
		return code;
	}

	/**
	 * FIXME: Create a Validator or Do not only persist in lowercase?
	 * @param code
	 */
	public void setCode(String code) {
		this.code = code.toLowerCase();
	}
}
