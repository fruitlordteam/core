package com.ftl.core.unused;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;

import com.ftl.core.service.CoreUserService;

public class CoreAuthenticationProvider implements AuthenticationProvider {

	@Autowired 
	private CoreUserService coreUserService;
	
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		UserDetails details = coreUserService.loadUserByUsername(authentication.getName());
		
		details.getPassword();
		
		return null;
	}

	@Override
	public boolean supports(Class<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

}
