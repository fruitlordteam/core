package com.ftl.core.service;

 import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

/**
 * Service to send emails with Spring-Email + Thymeleaf Implementation
 * <ul>
 * <li><a href=
 *
 'http://docs.spring.io/spring/docs/current/spring-framework-reference/html/mail.html'>
 * Spring Docs<a/></li>
 * <li><a href='http://www.thymeleaf.org/doc/articles/springmail.html'>Spring
 +
 * Thymeleaf<a/></li>
 * </ul>
 *
 * @author Petros Siatos
 *
 */
 abstract public class AbstractCoreMailService {

 @Autowired
 private JavaMailSender mailSender;

// @Autowired
// private SpringTemplateEngine templateEngine;
//
// private Context ctx;
 private String htmlFile;

 public AbstractCoreMailService(Locale locale, String htmlFile) {
// initThymeleafContext(locale);
 this.htmlFile = htmlFile;
 }

// private void initThymeleafContext(Locale locale) {
// this.ctx = new Context(locale);
// }

// protected String getHtmlContent() {
// return templateEngine.process(htmlFile, ctx);
// }

 abstract void prepareSender(JavaMailSender mailSender);

 abstract void prepareMessage(MimeMessageHelper messageHelper);

 }
