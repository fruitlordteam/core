package com.ftl.core.service;

import com.ftl.core.model.CoreModel;
import com.ftl.core.annotation.validation.UniqueModel;

/**
 * Implement this to use {@link UniqueModel} in your {@link CoreModel}
 * 
 * @author Petros Siatos
 *
 */
public interface IUniqueModelService {
	public Boolean exists(CoreModel model);
}