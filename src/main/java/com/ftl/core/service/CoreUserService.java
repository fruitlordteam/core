package com.ftl.core.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.ftl.core.dao.CoreUserDao;
import com.ftl.core.model.CoreModel;
import com.ftl.core.model.CoreUser;
import com.ftl.core.model.dto.UserRegisterInfo;

/**
 * Service for {@link CoreUser}
 * works with Spring Security by implementing {@link UserDetailsService}
 *  
 * @author Petros Siatos
 *
 */
@Service
public class CoreUserService extends CoreService<CoreUser, CoreUserDao> implements IUniqueModelService, UserDetailsService {
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	public Optional<CoreUser> findByLogin(String login) {
		return getDao().findByLogin(login);
	}
	
	public Optional<CoreUser> getByLogin(String login) {
		return getDao().getByLogin(login);
	}

	public CoreUser register(UserRegisterInfo registerInfo) {
		return save(getUser(registerInfo));
	}

	/**
	 * Returns a User from the given registerInfo
	 * Encrypts Password with {@link org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder}
	 * Adds User Role to user.
	 * @param registerInfo
	 * @return
	 */
	public CoreUser getUser(UserRegisterInfo registerInfo) {
		CoreUser user = new CoreUser();
		user.setLogin(registerInfo.getLogin());
		user.setPassword(passwordEncoder.encode(registerInfo.getPassword()));
		user.addUserRole();
		
		return user;
	}
	
	@Override
	public Boolean exists(CoreModel model) {
		return getDao().existsByLogin(((CoreUser) model).getLogin());
	}
	
	// UserDetailsService implementation
	@Override
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
		Optional<CoreUser> user = findByLogin(login);
		if (user.isPresent()) {
			return user.get();
		} else {
			throw new UsernameNotFoundException("Nope!");
		}
	}

}
