package com.ftl.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ftl.core.dao.ICoreDao;
import com.ftl.core.model.CoreModel;
import com.ftl.core.config.CoreConfig;

/**
 * All services that wish to use the database have to extend this
 * 
 * Uses Spring Data Repository
 * Remember to include {@link CoreConfig}
 * for your daos to work.
 * 
 * @author Petros Siatos
 * 
 * @param <M> the Model 
 * @param <D> the Dao
 */
@Transactional(propagation= Propagation.REQUIRED)
public abstract class CoreService<M extends CoreModel, D extends ICoreDao<M>> {

	private D dao;
	
	public M find(Long id) {
		return dao.findOne(id);
	}
	
	public Iterable<M> findAll() {
		return dao.findAll();
	}
	
	public Page<M> findAll(Pageable pageable) {
		return dao.findAll(pageable);
	}
	
	public M save(M model) {
		return dao.save(model);
	}
	
	public void delete(M model) {
		dao.delete(model);
	}
	
	public void delete(Long id) {
		dao.delete(id);
	}
	
//	public DataSet<M> findWithDatatablesCriterias(DatatablesCriterias cr) {
//		return CoreDandelionSpring.getDataSet(dao.findAll(CoreDandelionSpring.getPageable(cr)));
//	}
	
	@Autowired
	void setDao(D dao){
		this.dao = dao;
	}
	
	public D getDao() {
		return dao;
	}
}
