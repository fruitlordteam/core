package com.ftl.core.annotation;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.springframework.core.annotation.AliasFor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ftl.core.utils.CoreConstant;

@RequestMapping(method = RequestMethod.POST)
@Retention(RUNTIME)
@Target(METHOD)
public @interface Post {

	@AliasFor(annotation = RequestMapping.class, attribute = CoreConstant.SYMBOL_VALUE)
	String[] value() default {};
}
