package com.ftl.core.annotation.validation;

import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.ftl.core.service.CoreService;
import com.ftl.core.validation.UniqueModelValidator;

import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.RUNTIME;
import static java.lang.annotation.ElementType.TYPE;
import com.ftl.core.service.IUniqueModelService;

/**
 * Custom Hibernate Validator Annotation to check if a model is unique. You need
 * to Pass the corresponding Service that implements {@link IUniqueModelService}
 * 
 * @author Petros Siatos
 *
 */
@Target(TYPE)
@Retention(RUNTIME)
@Constraint(validatedBy = UniqueModelValidator.class)
public @interface UniqueModel {
	
	public static final String DEFAULT_MESSAGE = "{org.hibernate.validator.constraints.Unique.message}";

	String message() default DEFAULT_MESSAGE;

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

	/**
	 * The Model You want to validate.
	 * 
	 * @return
	 */
	// Class<? extends CoreModel> model();

	/**
	 * The Service that needs to implement {@link IUniqueModelService} TODO: Use
	 * Generics if possible.
	 * 
	 * @return
	 */
	Class<? extends CoreService<?, ?>> service();
}
