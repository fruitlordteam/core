package com.ftl.core.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ftl.core.unused.CoreAddress;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import static com.ftl.core.model.CoreModel.ID_SEQ;
import static com.ftl.core.config.CoreConfig.ALLOCATION_SIZE;

/**
 * Annotation for all persistent Core Models.
 * 
 * TODO: Find how it can be scanned.
 * @author Petros Siatos
 *
 */
@Entity
@Table(name= CoreAddress.TABLE)
@SequenceGenerator(name = ID_SEQ, sequenceName = CoreAddress.SEQ, allocationSize = ALLOCATION_SIZE)
@Retention(RUNTIME)
@Target(TYPE)
public @interface CoreEntity {
	
//	/**
//	 * Table Name
//	 */
//	@AliasFor(annotation = Table.class, attribute = CoreConstant.SYMBOL_NAME)
//	String table();
//	
//	/**
//	 * Sequence Name
//	 */
//	@AliasFor(annotation = SequenceGenerator.class, attribute = CoreConstant.SYMBOL_SEQUENCE_NAME)
//	String sequence();
}
