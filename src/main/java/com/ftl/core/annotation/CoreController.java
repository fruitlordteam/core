package com.ftl.core.annotation;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.springframework.core.annotation.AliasFor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ftl.core.utils.CoreConstant;

/**
 * Annotation for Controllers to reduce code from
 * <ul>
 * <li>@RequestMapping("users")</li>
 * <li>@Controller</li>
 * </ul>
 * to just
 * <ul>
 * <li>@CoreController("users")</li>
 * </ul>
 * 
 * <hr/>
 * <div>
 * *** WARNING ***
 * Eclipse @RequestMappings doesn't seem to find those (yet)
 * </div>
 * 
 * @author Petros Siatos
 *
 */
@RequestMapping
@Controller
@Retention(RUNTIME)
@Target(TYPE)
public @interface CoreController {

	@AliasFor(annotation = RequestMapping.class, attribute = CoreConstant.SYMBOL_VALUE)
	String[] value() default {};
}