package com.ftl.core.annotation;

import static java.lang.annotation.ElementType.METHOD;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.core.annotation.AliasFor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ftl.core.utils.CoreConstant;

@RequestMapping(method = RequestMethod.GET)
@Retention(RetentionPolicy.RUNTIME)
@Target(METHOD)
public @interface Get {

	@AliasFor(annotation = RequestMapping.class, attribute = CoreConstant.SYMBOL_VALUE)
	String[] value() default {};
}
