package com.ftl.core.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.ftl.core.annotation.validation.UniqueModel;
import com.ftl.core.model.CoreModel;

import com.ftl.core.service.IUniqueModelService;
/**
 * Validator to check if this model already exists in database.
 * <br/>
 * Your Service needs to implement {@link IUniqueModelService}
 * <br/>
 * <br/>
 * http://codingexplained.com/coding/java/hibernate/unique-field-validation-using-hibernate-spring
 * @author Petros Siatos
 *
 */
public class UniqueModelValidator implements ConstraintValidator<UniqueModel, CoreModel> {
	
	@Autowired
	private ApplicationContext context;
	
	private IUniqueModelService service;
	
	@Override
	public void initialize(UniqueModel annotation) {
		
		// Ignore on Persist
		// TODO: Fix this shit or remove this Validator completely.
		if (context != null) {
			System.out.println("UniqueModel init onValidate");
			service = (IUniqueModelService) context.getBean(annotation.service());
		} else {
			System.out.println("UniqueModel init onPersist");
		}
	}

	@Override
	public boolean isValid(CoreModel value, ConstraintValidatorContext validatorContext) {
		
		// Ignore on Persist
		// TODO: Fix this shit or remove this Validator completely.
		if (context != null) {
			System.out.println("UniqueModel isValid onValidate");
			value.getClass();
			return !service.exists(value);
		} else {
			System.out.println("UniqueModel isValid onPersist");
			return Boolean.TRUE;
		}
	}
	
}
