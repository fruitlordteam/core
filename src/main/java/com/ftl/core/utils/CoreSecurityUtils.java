package com.ftl.core.utils;

import java.util.Optional;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.ftl.core.model.CoreUser;
import com.ftl.core.model.UserRoleType;

public class CoreSecurityUtils {
	
	public static final String ANONYMOUS_USER = "anonymousUser";
	
	public static final String ROLE_ADMIN 		= "ROLE_ADMIN";
	public static final String ROLE_MODERATOR 	= "ROLE_MODERATOR";
	public static final String ROLE_USER		= "ROLE_USER";
	
	public static final String HAS_ROLE_ADMIN	= "hasRole('ROLE_ADMIN')";
	public static final String HAS_ROLE_USER = "hasRole('ROLE_USER')";; 

	
	/**
	 * Returns the current user logged in.
	 * @return
	 */
	public static Optional<CoreUser> getLoggedUser() {
		
		Optional<CoreUser> user = Optional.empty();
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		if (auth != null && auth.isAuthenticated() && !ANONYMOUS_USER.equals(auth.getPrincipal())) {
			user = Optional.of(((CoreUser) auth.getPrincipal()));
		}
		
	    return user;
	}
	
	/**
	 * Returns true if the given user is currently logged in.
	 * @param user
	 * @return
	 */
	public static boolean isLoggedIn(CoreUser user) {
		return getLoggedUser().map(user::equals).orElse(false);
	}
	
	/**
	 * Utility function to turn enumeration to SPRING Security role
	 * by adding prefix 'ROLE_' 
	 * @param role
	 * @return
	 */
	public static String getRole(UserRoleType role) {
		return String.join(CoreConstant.STR_UNDERSCORE,
				 CoreConstant.SYMBOL_ROLE.toUpperCase(),
				 role.toString());
	}

}

