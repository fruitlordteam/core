package com.ftl.core.utils;

import static com.ftl.core.utils.CoreConstant.STR_AMPERSAND;
import static com.ftl.core.utils.CoreConstant.STR_EQUALS;
import static com.ftl.core.utils.CoreConstant.STR_COMMA;
import static com.ftl.core.utils.CoreConstant.SYMBOL_SORT;

import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

public class CoreSpring {

	/**
	 * Turn {@link Sort} to get request parameters string.
	 * @param sort
	 * @return
	 */
	public static final String getUrlProperty(Sort sort) {
		
		return StreamSupport.stream(sort.spliterator(), false)
			.map(o -> String.join(STR_EQUALS, SYMBOL_SORT, 
					o.isAscending() ? o.getProperty() : 
						String.join(STR_COMMA, o.getProperty(), Direction.DESC.toString().toLowerCase())))
			.collect(Collectors.joining(STR_AMPERSAND));
	}
}
			