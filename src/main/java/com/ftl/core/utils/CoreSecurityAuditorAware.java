package com.ftl.core.utils;

import org.springframework.data.domain.AuditorAware;

import com.ftl.core.model.CoreUser;

/**
 * Auditor Implementatin using Spring Security to get
 * logged in CoreUser.
 * @author Petros Siatos
 *
 */
public class CoreSecurityAuditorAware implements AuditorAware<CoreUser> {

	public static final String ANONYMOUS_USER = "anonymousUser";
	
	public CoreUser getCurrentAuditor() {
		return CoreSecurityUtils.getLoggedUser().orElse(null);
	}
}