package com.ftl.core.utils;

public class CoreConstant {

	public static final String HTML5 = "HTML5";
	public static final String UTF8	= "UTF-8";
	
	// Warnings
	public static final String WARNING_UNCHECKED = "unchecked";
	public static final String WARNING_RAWTYPES = "rawtypes";
	public static final String WARNING_DEPRECATION = "deprecation";
	
	// SYMBOLS (Ruby like)
	public static final String SYMBOL_ID 		= "id";
	public static final String SYMBOL_VALUE 	= "value";
	public static final String SYMBOL_NAME 		= "name";
	public static final String SYMBOL_PATH 		= "path";
	public static final String SYMBOL_TABLE		= "table";
	public static final String SYMBOL_ROLE 		= "role";
	public static final String SYMBOL_SORT		= "sort";
	
	// String
	public static final String STR_SPLITTER_1= " - ";
	public static final String STR_SPLITTER_2= " / ";
	
	public static final String STR_GAP = "";
	public static final String STR_EMPTY = "";
	public static final String STR_COMMA = ",";
	public static final String STR_SLASH = "/";
	public static final String STR_DOT = ".";
	public static final String STR_UNDERSCORE = "_";
	public static final String STR_EQUALS = "=";
	public static final String STR_AMPERSAND= "&";
	public static final String STR_QUESTIONMARK= "?";
	
	// Characters
	public static final Character CH_SLASH = '/';
	
	// JPA / Hibernate
	public static final String SYMBOL_SEQUENCE_NAME			= "sequenceName";
	public static final String SYMBOL_VALUE_COLUMN_NAME 	= "valueColumnName";
	
	public static final String COLUMN_DEFINITION_TIMESTAMP = "timestamp";
	
	
	// SPRING SCOPE
	public static final String  SCOPE_PROTOTYPE = "prototype",
								SCOPE_SINGLETON = "singleton",
								SCOPE_REQUEST 	= "request",
								SCOPE_SESSION 	= "session";
	
	// PROFILE
	public static final String PROFILE_DEVELOPMENT 	= "development";
	public static final String PROFILE_HEROKU		= "heroku";
	public static final String PROFILE_DEPLOYMENT 	= "deployment";
	public static final String PROFILE_TEST			= "test";
	public static final String PROFILE_PC_PETROS	= "pc-petros";
	public static final String PROFILE_PC_EVA		= "pc-eva";
	
	// REGEX PATTERN
	public static final String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
//	public static final String PATTERN_PASSWORD = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})";
//	public static final String PATTERN_LOGIN = "(?=.{6,20}$)[A-Za-z0-9]";
	
	
}
