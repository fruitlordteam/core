//package com.ftl.core.utils;
//
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.PageRequest;
//import org.springframework.data.domain.Pageable;
//
//import com.ftl.core.model.CoreModel;
//import com.github.dandelion.datatables.core.ajax.DataSet;
//import com.github.dandelion.datatables.core.ajax.DatatablesCriterias;
//
///**
// * Utility functions for Spring Page and Dandelion Datatables.
// * @author Petros Siatos
// *
// */
//public class CoreDandelionSpring {
//
//	public static final <M extends CoreModel> DataSet<M> getDataSet(Page<M> page) {
//		return new DataSet<M>(page.getContent(), page.getTotalElements(), new Long(page.getNumberOfElements()));
//	}
//	
//	public static final <M extends CoreModel> Pageable getPageable(DatatablesCriterias cr) {
//		return new PageRequest(cr.getStart(), cr.getLength());
//	}
//	
//}
