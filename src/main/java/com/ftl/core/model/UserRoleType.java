package com.ftl.core.model;

/**
 * Enumeration for the User Roles.
 * @author Petros Siatos
 *
 */
public enum UserRoleType {
	USER, MODERATOR, ADMIN;
}