package com.ftl.core.model;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.FetchProfile;
import org.hibernate.annotations.FetchProfile.FetchOverride;
import org.hibernate.annotations.NaturalId;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ftl.core.annotation.validation.UniqueModel;
import com.ftl.core.service.CoreUserService;
import com.ftl.core.utils.CoreConstant;

/**
 * A simple user for login containing login/name/password/email.
 * 
 * Uses Spring Security.
 * <a href='http://docs.spring.io/spring-security/site/docs/4.0.4.CI-SNAPSHOT/reference/htmlsingle/#appendix-schema'>Spring Doc</a>
 * 
 * @author Petros Siatos
 *
 */
@Entity
@Table(name= CoreUser.TABLE)
@SequenceGenerator(name = CoreUser.ID_SEQ, sequenceName = CoreUser.SEQ, allocationSize = 1)
@UniqueModel(service = CoreUserService.class)
@NamedEntityGraph(name = CoreUser.ENTITY_GRAPH_WITH_ROLES, attributeNodes = @NamedAttributeNode(CoreUser.ROLES))
@FetchProfile(
	name = CoreUser.FETCH_PROFILE_WITH_ROLES,
	fetchOverrides={
		@FetchOverride(
			entity=CoreUser.class,
			association= CoreUser.ROLES,
			mode= FetchMode.JOIN
		)
	}
)
public class CoreUser extends CoreModel implements UserDetails{

	private static final long serialVersionUID = 1L;
	
	public static final String TABLE		= "core_user";
	public static final String SEQ 			= "core_user_seq";
	
	// Entity Graphs / Fetch Profiles
	public static final String ENTITY_GRAPH_WITH_ROLES = "EG.w.roles";
	
	/**
	 * <a href="http://docs.jboss.org/hibernate/orm/5.0/userGuide/en-US/html_single/"> Hibernate Doc </a> 
	 * Example 9.5. 
	 */
	public static final String FETCH_PROFILE_WITH_ROLES = "FP.w.roles";
	
	// Attributes
	public static final String NAME 		= "name";
	public static final String LOGIN 		= "login";
	public static final String PASSWORD 	= "password";
	public static final String EMAIL 		= "email";
	public static final String ENABLED		= "enabled";
	public static final String ROLES		= "roles";
	
	// Validation
	public static final String ERROR_INVALID_EMAIL 		= "invalid.email";
	
	private String name;
	
	@NotEmpty
	@NaturalId
	@Column(unique = true)
	protected String login;
	
	@NotEmpty
	private String password;
	
	@Pattern(regexp = CoreConstant.PATTERN_EMAIL, message = ERROR_INVALID_EMAIL)
	@Email
	@Column(unique = true)
	private String email;
	
	private Boolean enabled = Boolean.TRUE;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = CoreUserRole.USER)
	@Cascade(CascadeType.ALL)
	private Set<CoreUserRole> roles = new HashSet<CoreUserRole>();

	public CoreUser() {}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	/**
	 * DO NOT USE (SETTER ONLY FOR HIBERNATE)
	 * Use {@link com.ftl.core.service.CoreUserService#encryptAndSetPassword} instead.
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	
	@JsonIgnore
	public Set<CoreUserRole> getRoles() {
		return roles;
	}

	public void setRoles(Set<CoreUserRole> roles) {
		this.roles = roles;
	}

	// Utility Functions
	@Override
	public String toString() {
		return login;
	}
	
//	/**
//	 *  Suggested by Hibernate Example 2.8. Better equals/hashCode with natural-id
//	 *  <a> http://docs.jboss.org/hibernate/orm/5.0/userGuide/en-US/html_single/ </a>
//	 */
//	@Override
//	public int hashCode() {
//		assert login != null;
//		return login.hashCode();
//	}
//	
//	/**
//	 *  Suggested by Hibernate Example 2.8. Better equals/hashCode with natural-id
//	 *  <a> http://docs.jboss.org/hibernate/orm/5.0/userGuide/en-US/html_single/ </a>
//	 */
//	@Override
//	public boolean equals(Object o) {
//		if ( this == o ) {
//			return true;
//		}
//		if (o == null) {
//			return false;
//		}
//		if ( !( o instanceof CoreUser ) ) {
//			return false;
//		}
//
//		final CoreUser other = (CoreUser) o;
//		assert login != null;
//		assert other.login != null;
//
//		return login.equals( other.login );
//	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CoreUser other = (CoreUser) obj;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		return true;
	}

	public void addUserRole() {
		roles.add(new CoreUserRole(this, UserRoleType.USER));
	}
	
	public void addAdminRole() {
		roles.add(new CoreUserRole(this, UserRoleType.ADMIN));
	}
	
	// Spring Security - UserDetails Implementation
	@Override
	@JsonIgnore
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return roles;
	}

	@Override
	public String getUsername() {
		return login;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

}
