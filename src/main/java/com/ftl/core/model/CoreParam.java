package com.ftl.core.model;

/**
 * A simple Attribute/Value parameter model.
 * 
 * @author Petros Siatos
 *
 * @param <V>
 *            the value of the attribute
 */
public class CoreParam<V extends Object> {

	/**
	 * Factory Method
	 * @param attribute
	 * @param value
	 * @return
	 */
	public static <V> CoreParam<V> createCoreParam(String attribute, V value) {
		return new CoreParam<V>(attribute, value);
	}

	private String attribute;
	private V value;

	private CoreParam(String attribute, V value) {
		super();
		this.attribute = attribute;
		this.value = value;
	}

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public V getValue() {
		return value;
	}

	public void setValue(V value) {
		this.value = value;
	}
}
