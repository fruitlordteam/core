package com.ftl.core.model;

public enum ProfileType {
	DEVELOPMENT, DEPLOYMENT, TEST, EVA, PETROS;
}
