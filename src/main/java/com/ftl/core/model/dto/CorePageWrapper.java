package com.ftl.core.model.dto;

import static com.ftl.core.utils.CoreConstant.CH_SLASH;
import static com.ftl.core.utils.CoreConstant.STR_AMPERSAND;
import static com.ftl.core.utils.CoreConstant.STR_EQUALS;
import static com.ftl.core.utils.CoreConstant.STR_QUESTIONMARK;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.util.StringUtils;

import com.ftl.core.model.CoreModel;
import com.ftl.core.model.CoreParam;
import com.ftl.core.utils.CoreSpring;

/**
 * Wrapper object for Spring's pagination {@link Page}
 * @author Petros Siatos
 *
 * @param <M>
 */
public class CorePageWrapper<M extends CoreModel> {

	private Page<M> page;
	private String url;
	
	/**
	 * The html id of the area to be replaced by the paging ajax calls.
	 */
	private String ajaxArea;
	
	/**
	 * Javascript function to be called after a successful ajax call.
	 * (to redraw something for example)
	 */
	private String afterSuccess;

	public CorePageWrapper() {
		super();
	}
	
	public CorePageWrapper(Page<M> page, String url, String ajaxArea, CoreParam<?>... params) {
		super();
		this.page = page;
		this.url = url;
		this.ajaxArea = ajaxArea;
		setUrlParams(params);
	}
	
	public Page<M> getPage() {
		return page;
	}

	public void setPage(Page<M> page) {
		this.page = page;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getAjaxArea() {
		return ajaxArea;
	}

	public void setAjaxArea(String ajaxArea) {
		this.ajaxArea = ajaxArea;
	}
	
	public String getAfterSuccess() {
		return afterSuccess;
	}

	public void setAfterSuccess(String afterSuccess) {
		this.afterSuccess = afterSuccess;
	}

	/**
	 * Set Url Get Params including page's {@link Sort}.
	 * @param params
	 */
	private void setUrlParams(CoreParam<?>... params) {
		
		assert params != null;
		
		url = String.join(STR_QUESTIONMARK, StringUtils.trimTrailingCharacter(url, CH_SLASH), 
				Stream.of(params)
					.map(p -> String.join(STR_EQUALS, p.getAttribute(), String.valueOf(p.getValue())))
					.collect(Collectors.joining(STR_AMPERSAND)));
		
		assert page != null;
		
		if (page.getSort() != null) {
			url = String.join(STR_AMPERSAND, url, CoreSpring.getUrlProperty(page.getSort()));
		}
	}
	
	public boolean isFirst() {
		return page.isFirst();
	}
	
	public boolean isLast() {
		return page.isLast();
	}
	
	public boolean hasNext() {
		return page.hasNext();
	}
	
	public boolean hasPrevious() {
		return page.hasPrevious();
	}
	
	public Pageable nextPageable() {
		return page.nextPageable();
	}

	public Pageable previousPageable() {
		return page.previousPageable();
	}
	
	public boolean hasContent() {
		return page.hasContent();
	}

	public List<M> getContent() {
		return page.getContent();
	}
	
	public int getNumber() {
		return page.getNumber();
	}
	
	public int getSize() {
		return page.getSize();
	}
		
	public int getNumberOfElements() {
		return page.getNumberOfElements();	
	}
	
	public long getTotalElements() {
		return page.getTotalElements();
	}
	
	public long getTotalPages() {
		return page.getTotalPages();
	}
	
}
