package com.ftl.core.model.dto;

import com.ftl.core.model.CoreUser;
import com.ftl.core.utils.CoreConstant;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Register Information Model for a {@link CoreUser}.
 * Includes Validation.
 * 
 * @author Petros Siatos
 *
 */
public class UserRegisterInfo {
	
	// Validation
	public static final String ERROR_INVALID_PASSWORD = "invalid.password";
	public static final String ERROR_INVALID_EMAIL 	  = "invalid.email";
	
	@NotEmpty
	// TODO: create the patterns and tests to validate.
//	@Pattern(regexp = CoreConstant.PATTERN_LOGIN, message = ERROR_INVALID_LOGIN)
	private String login;
	
	@NotEmpty
//	@Pattern(regexp = CoreConstant.PATTERN_PASSWORD, message = ERROR_INVALID_PASSWORD)
	private String password;
	
	@Pattern(regexp = CoreConstant.PATTERN_EMAIL, message = ERROR_INVALID_EMAIL)
	private String email;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
