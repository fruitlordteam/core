package com.ftl.core.model;

import java.time.LocalDateTime;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import com.ftl.core.utils.CoreConstant;

/**
 * All database models needs to extend this.
 * 
 * Consists of a unique ID and a description. 
 *  
 * @author Petros Siatos
 *
 */
@MappedSuperclass
public abstract class CoreModel {
 
	public static final String ID_SEQ = "ID_SEQ";
	
	// Attributes
	public static final String ID	= "id";
	public static final String DESC	= "description";
	public static final String CREATED_BY			= "createdBy";
	public static final String LAST_MODIFIED_BY		= "lastModifiedBy";
	public static final String CREATED_DATE			= "createdDate";
	public static final String UPDATE_DATE			= "updateDate";
	
	@Id
	@GeneratedValue(generator = ID_SEQ)
	protected Long id;
	
	protected String 		description;
	
	// Audit
	@CreatedBy
	@ManyToOne
	@Basic(fetch = FetchType.LAZY)
	protected CoreUser createdBy;
	
	@LastModifiedBy
	@ManyToOne
	@Basic(fetch = FetchType.LAZY)
	protected CoreUser lastModifiedBy;
	
//	@Temporal(TemporalType.TIMESTAMP)
	@CreatedDate
	@Column(columnDefinition = CoreConstant.COLUMN_DEFINITION_TIMESTAMP)
	@Basic(fetch = FetchType.LAZY)
	protected LocalDateTime createdDate;
	
//	@Temporal(TemporalType.TIMESTAMP)
	@LastModifiedDate
	@Column(columnDefinition = CoreConstant.COLUMN_DEFINITION_TIMESTAMP)
	@Basic(fetch = FetchType.LAZY)
	protected LocalDateTime updateDate;
	
	// Getters - Setters
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public CoreUser getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(CoreUser createdBy) {
		this.createdBy = createdBy;
	}

	public CoreUser getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(CoreUser lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public LocalDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public LocalDateTime getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}

	// Utility Functions
	@Override
	public String toString() {
		return description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CoreModel other = (CoreModel) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
