package com.ftl.core.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.util.StringUtils;

import com.ftl.core.utils.CoreSecurityUtils;

/**
 * Model that associates {@link CoreUser} and {@link UserRoleType}
 * Uses Spring Security.
 * <a href='http://docs.spring.io/spring-security/site/docs/4.0.4.CI-SNAPSHOT/reference/htmlsingle/#appendix-schema'>Spring Doc</a>
 * @author Petros Siatos
 *
 */
@Entity
@Table(name = CoreUserRole.TABLE, uniqueConstraints = @UniqueConstraint(columnNames = {CoreUserRole.COLUMN_USER, CoreUserRole.ROLE}))
@SequenceGenerator(name = CoreUserRole.ID_SEQ, sequenceName = CoreUserRole.SEQ, allocationSize = 1)
public class CoreUserRole extends CoreModel implements GrantedAuthority {

	private static final long serialVersionUID = 1L;
	
	public static final String ENTITY		= "coreUserRole";
	public static final String TABLE		= "core_user_role";
	public static final String SEQ 			= "core_user_role_seq";
	
	// Attributes
	public static final String USER 		= "user";
	public static final String ROLE 		= "role";
	
	// Column Names
	public static final String COLUMN_USER 	= "user_id";
	
	@NotNull
	@ManyToOne
	private CoreUser user;
	
	@NotNull
	@Enumerated(EnumType.STRING)
	private UserRoleType role;

	public CoreUserRole() { super(); }

	public CoreUserRole(CoreUser user, UserRoleType role) {
		super();
		this.user = user;
		this.role = role;
	}

	public CoreUser getUser() {
		return user;
	}

	public void setUser(CoreUser user) {
		this.user = user;
	}
	
	public UserRoleType getRole() {
		return role;
	}

	public void setRole(UserRoleType role) {
		this.role = role;
	}

	// Utility Functions
	@Override
	public String toString() {
		return StringUtils.capitalize(role.toString().toLowerCase());
	}
	
	// Spring Security - GrantedAuthority implementation
	@Override
	public String getAuthority() {
		return CoreSecurityUtils.getRole(role);
	}
	
}
