package com.ftl.core.model;

/**
 * A simple page definition model
 * @author Petros Siatos
 *
 */
public class CorePage {

	private String descr;
	private String uri;
	
	public CorePage(String descr, String uri) {
		super();
		setDescr(descr);
		setUri(uri);
	}
	
//	// TODO: use tuple or cute way to pass uri arguments?
//	public CorePage(String descr, Class<?> controllerClass, String methodName, Object[] argValues) {
//		super();
//		setDescr(descr);
//		setUri(controllerClass, methodName, argValues);
//	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri =  uri;
	}
	
//	public void setUri(Class<?> controllerClass, String methodName, Object[] argValues) {
//		uri = MvcUriComponentsBuilder.fromMethodName(controllerClass, methodName, new Object()).build().toUriString();
//	}

}
